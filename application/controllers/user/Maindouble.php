 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maindouble extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		
		$this->load->model("user/main_user", "mu");
		$this->load->library("response_message");
	}

	public function index(){
		$id_admin = "AD2018100001";
		$id_setting = "1";

		$data_alpha = $this->mu->get_alpha(array("id_setting"=>$id_setting));

		$alpha 	= (double)$data_alpha["alpha"];
		$beta 	= (double)$data_alpha["beta"];

		$p_forecast = 5;

		$array_analisis = array();

		$data_all = $this->mu->get_laporan_all($id_admin);

		$no = 1;
		$key_before = 0;
		foreach ($data_all as $key => $value) {
			if($no == 1){
				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$value->penjualan,
										"tt"=>0,
										"yt_est"=>$value->penjualan,
										"residual"=>0
									);
			}else {
				$lt = $alpha*(double)$value->penjualan+(1-$alpha)*((double)$array_analisis[$no-1]["lt"] + (double)$array_analisis[$no-1]["tt"]);

				$tt = $beta * ($lt - (double)$array_analisis[$no-1]["lt"]) + (1 - $beta) * (double)$array_analisis[$no-1]["tt"];

				$yt_est = $array_analisis[$no-1]["yt"];
				if($array_analisis[$no-1]["tt"] != 0){
					$yt_est = (double)$array_analisis[$no-1]["lt"] + 1 * (double)$array_analisis[$no-1]["tt"];
				}

				$residual = (double)$value->penjualan - $yt_est;

				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$lt,

										"tt"=>$tt,
										"yt_est"=>$yt_est,
										"residual"=>$residual
									);

			}

			$lt_end = $array_analisis[$no]["lt"];
			$tt_end = $array_analisis[$no]["tt"];
			$tgl_end= $value->tgl;

			$no++;			
		}

		$array_forecast = array();
		for ($i=1; $i <= $p_forecast ; $i++) { 
			$yt_est = (double)$lt_end + $i * (double)$tt_end;

			$array_forecast[$i] = array(
										"tgl"=>date('Y-m-d', strtotime('+'.$i.' days', strtotime($tgl_end))),
										"t"=>$i,
										"yt"=>0,
										"lt"=>0,

										"tt"=>0,
										"yt_est"=>$yt_est,
										"residual"=>0
									);

		}

		print_r($array_analisis);
		print_r($array_forecast);
	}
}
