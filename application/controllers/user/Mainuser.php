<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainuser extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		
		$this->load->model("user/main_user", "mu");
		$this->load->library("response_message");

		// if($this->session->userdata("indo_log")["is_log"] != 1){
  //           redirect(base_url());
  //       }else{
  //       	if($this->session->userdata("indo_log")["jenis_admin"] != 1){
  //               redirect(base_url());
  //           }
  //       }
        // print_r($this->session->userdata("indo_log")["jenis_admin"]);
	}

	public function index(){
		$data["page"] = "home";
		$this->load->view('index', $data);
	}

#==================================================================================================== Penjualan =======================================================================================
#------------------------------------------------------------------------------------------------------- Main -------------------------------------------------------------------------------------------
#==================================================================================================== Penjualan =======================================================================================
	public function index_oi(){
		$data["page"] = "penjualan";
		$this->load->view('index_admin', $data);
	}

	public function index_penjualan(){
		// $periode = date("m");
		// $th = date("Y");
		// $id_admin = $_SESSION["indo_log"]["id_admin"];

		// $this->indexmain_penjualan($periode, $th);
		$data["page"] = "penjualan";

		$id_admin = $this->session->userdata("indo_log")["id_admin"];

		$data["penjualan"] = $this->mu->get_laporan_all($id_admin);

		$this->load->view('index_admin', $data);
	}

	public function indexing_penjualan(){
		$periode = $this->input->post("periode");
		$th = $this->input->post("th");

		// print_r($_POST);

		$this->indexmain_penjualan($periode, $th);
	}

	public function indexmain_penjualan($periode, $th){
		$data["page"] = "penjualan";

		$id_admin = $this->session->userdata("indo_log")["id_admin"];

		$data["penjualan"] = $this->mu->get_laporan_where($id_admin, (int)$periode, $th);

		// print_r("<pre>");
		// print_r($data["penjualan"]);

		$graph_data = array();
		$key_graph = 0;
		foreach ($data["penjualan"] as $r_data => $v_data) {
			$graph_data[$key_graph]["period"] = explode("-", $v_data->tgl)[2];
			$graph_data[$key_graph]["val_data"] = (int)$v_data->sales_net;
			$key_graph++;
		}

		
		$data["graph_data"] = json_encode($graph_data);
		// print_r($th);

		// print_r(json_encode($graph_data));

		$this->load->view('index_admin', $data);
	}

	
	public function validaiton_form(){
		$config_val_input = array(
            array(
                'field'=>'tgl',
                'label'=>'Tanggal Input',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'sales_net',
                'label'=>'Sales Net',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'struk',
                'label'=>'Struk',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'penggantian',
                'label'=>'Penggantian',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'variance',
                'label'=>'Variance',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'disc',
                'label'=>'Discount',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'dsi_ini',
                'label'=>'DSI Hari Ini',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_penjualan(){

		print_r("<pre>");
		if($this->validaiton_form()){
			$tgl = $this->input->post("tgl");
			$sales_net = $this->input->post("sales_net");
			$struk = $this->input->post("struk");
			$penggantian = $this->input->post("penggantian");
			$variance = $this->input->post("variance");
			$disc = $this->input->post("disc");
			$dsi_ini = $this->input->post("dsi_ini");

			$id_admin = $this->session->userdata("indo_log")["id_admin"];

			$data = array(
						"id_lap"=>"",
						"id_admin"=>$id_admin,
						"tgl"=>$tgl,
						"sales_net"=>$sales_net,
						"struk"=>$struk,
						"pergantian"=>$penggantian,
						"variance"=>$variance,
						"discount"=>$disc,
						"dsi_ini"=>$dsi_ini
					);



			print_r($data);
			$insert = $this->mu->insert_laporan($data);
			if($insert){
				$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
				$detail_msg = null;
			}else {
				$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
				$detail_msg = null;
			}

		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
			$detail_msg = array(
					"tgl" => form_error("tgl"),
					"sales_net" => form_error("sales_net"),
					"struk" => form_error("struk"),
					"penggantian" => form_error("penggantian"),
					"variance" => form_error("variance"),
					"disc" => form_error("disc"),
					"dsi_ini" => form_error("dsi_ini")
				);
		}

		$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
		$this->session->set_flashdata("response_laporan", $msg_array);

		// print_r($msg_array);
		redirect(base_url()."admin/penjualan");
	}

	public function delete_penjualan($id_penjualan){
		if($this->mu->delete_laporan(array("id_lap"=>$id_penjualan))){
			$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
			$detail_msg = null;
		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
			$detail_msg = null;
		}
		$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
		print_r($msg_array);
		redirect(base_url()."admin/penjualan");
	}

	public function index_up_penjualan(){
		$id_lap = $this->input->post("id_lap");
		$data["status"] = false;
		$data["val"] = null;
			
		if(!empty($this->mu->get_laporan_where_id($id_lap))){
			$data["status"] = true;
			$data["val"] = $this->mu->get_laporan_where_id($id_lap);
		}

		print_r(json_encode($data));
	}

	public function up_penjualan(){
		if($this->validaiton_form()){
			$id_lap = $this->input->post("id_lap");

			$tgl = $this->input->post("tgl");
			$sales_net = $this->input->post("sales_net");
			$struk = $this->input->post("struk");
			$penggantian = $this->input->post("penggantian");
			$variance = $this->input->post("variance");
			$disc = $this->input->post("disc");
			$dsi_ini = $this->input->post("dsi_ini");

			$id_admin = $this->session->userdata("indo_log")["id_admin"];

			$data = array(
						"tgl" => $tgl,
						"sales_net" => $sales_net,
						"struk" => $struk,
						"pergantian" => $penggantian,
						"variance" => $variance,
						"discount" => $disc,
						"dsi_ini" => $dsi_ini
					);

			$where = array(
						"id_lap" => $id_lap
					);


			$update = $this->mu->update_laporan($data,$where);
			if($insert){
				$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
				$detail_msg = null;
			}else {
				$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
				$detail_msg = null;
			}

		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
			$detail_msg = array(
					"tgl" => form_error("tgl"),
					"sales_net" => form_error("sales_net"),
					"struk" => form_error("struk"),
					"penggantian" => form_error("penggantian"),
					"variance" => form_error("variance"),
					"disc" => form_error("disc"),
					"dsi_ini" => form_error("dsi_ini")
				);
		}

		$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
		$this->session->set_flashdata("response_laporan", $msg_array);

		// print_r($msg_array);
		redirect(base_url()."admin/penjualan");
	}


#==================================================================================================== Penjualan =======================================================================================
#------------------------------------------------------------------------------------------------------- Main -------------------------------------------------------------------------------------------
#==================================================================================================== Penjualan =======================================================================================


	public function index_laporan(){
		$data["page"] = "laporan";
		$this->load->view('index_admin',$data);
	}

}
