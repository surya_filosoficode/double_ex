                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Single Exponensial Forecasting</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                
            </div> -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Data Perhitungan</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example24" border="1" width="100%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <?php
                                                    if($array_alpha){
                                                        $width_head = 100 / count($array_alpha);

                                                        $str_header = "<th>Tanggal</th>
                                                                        <th>Sales Net</th>";
                                                        foreach ($array_alpha as $key => $value) {
                                                            $str_header .= "<th>aplha (".$value.")</th>";
                                                        }

                                                        print_r($str_header);
                                                    }
                                                ?>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <?php print_r($str_header); ?>
                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">
                                            <?php
                                                if($data_analisa){
                                                    foreach ($data_analisa as $r_data_analisa => $v_data_analisa) {
                                                        // print_r("<pre>");
                                                        // print_r($v_data_analisa);
                                                        print_r("<tr>
                                                                    <td align=\"center\">".$v_data_analisa["tgl"]."</td>
                                                                    <td align=\"center\">".number_format($v_data_analisa["base_data"], 2, ".", ",")."</td>");
                                                        foreach ($array_alpha as $key => $value) {
                                                            print_r("   <td align=\"right\">".number_format($v_data_analisa["item"][$key], 2, ".", ",")."</td>");
                                                        }
                                                        print_r("</tr>");
                                                    }

                                                    // print_r($str_header);
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Data Forecasting</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example23" border="1" width="100%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <?php
                                                    if($array_alpha){
                                                        $width_head = 100 / count($array_alpha)+3;

                                                        $str_header = "<th>Tanggal</th>
                                                                        <th>Sales Net</th>";
                                                        foreach ($array_alpha as $key => $value) {
                                                            $str_header .= "<th>aplha (".$value.")</th>";
                                                        }
                                                        $str_header .= "<th style=\"background: #9fc7f3;\">Target Penjualan</th>";

                                                        print_r($str_header);
                                                    }
                                                ?>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <?php print_r($str_header); ?>
                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">
                                            <?php
                                                if($data_forecast){
                                                    foreach ($data_forecast as $r_data_forecast => $v_data_forecast) {
                                                        // print_r("<pre>");
                                                        // print_r($v_data_forecast);
                                                        print_r("<tr>
                                                                    <td align=\"center\">".$v_data_forecast["tgl"]."</td>
                                                                    <td align=\"center\">-</td>");

                                                        $tmp_count = 0;
                                                        $count_array = 0;
                                                        foreach ($array_alpha as $key => $value) {
                                                            print_r("   <td align=\"right\">".number_format($v_data_forecast["item"][$key], 2, ".", ",")."</td>");
                                                            $tmp_count += $v_data_forecast["item"][$key];

                                                            $count_array++;
                                                        }

                                                        print_r("   <td align=\"right\" style=\"background: #9fc7f3;\">".number_format(($tmp_count/$count_array), 2, ".", ",")."</td>");
                                                        print_r("</tr>");

                                                        // print_r($count_array);
                                                    }

                                                    // print_r($str_header);
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            