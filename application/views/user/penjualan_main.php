            <?php
            // print_r("<pre>");
                // print_r($penjualan);
            ?>

                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Penjualan</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Input Data Laporan Penjualan</h4>
                            </div>
                            <div class="card-body">
                                <form action="<?= base_url()."user/mainuser/insert_penjualan"?>" method="post" class="form-horizontal">
                                    <div class="form-body">
                                        <h3 class="box-title">Data 1</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tangal Input</label>
                                                    <div class="col-md-9">
                                                        <input type="date" class="form-control" id="tgl" name="tgl" value="<?php echo date("Y-m-d");?>" required="">
                                                        <p id="ex_tgl"></p>
                                                    </div>
                                                    

                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Sales Net</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" placeholder="300000" id="sales_net" name="sales_net" required="">
                                                        <p id="ex_sales_net"></p>
                                                    </div>
                                                    

                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Struk</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" placeholder="409" id="struk" name="struk" required="">
                                                        <p id="ex_struk"></p>
                                                    </div>
                                                    

                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Penggantian</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" placeholder="700000" id="penggantian" name="penggantian" required="">
                                                        <p id="ex_penggantian"></p>
                                                    </div>
                                                    

                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Variance</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" placeholder="800000" id="variance" name="variance" required="">
                                                        <p id="ex_variance"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        
                                        <h3 class="box-title">Data 2</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Discount</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" placeholder="700000" id="disc" name="disc" required="">
                                                        <p id="ex_disc"></p>
                                                    </div>
                                                    

                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">DSI Hari Ini</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" placeholder="800000" id="dsi_ini" name="dsi_ini" required="">
                                                        <p id="ex_dsi"></p>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                        <button type="button" class="btn btn-danger">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Grafik Data Sales Net</h4>
                                <ul class="list-inline text-center m-t-40">
                                    <li>
                                        <h5><i class="fa fa-circle m-r-5 text-info"></i>Sales Net</h5>
                                    </li>
                                </ul>
                                <div id="extra-area-chart" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">
                            
                <!-- -----------------------------------------------------------------------------------------Data Filter-------------------------------------------------------------------------------------- -->
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Filter Data</h4>
                            </div>
                            <div class="card-body">
                                <form method="post" action="<?= base_url()."user/mainuser/indexing_penjualan";?>">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Periode</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="periode" id="periode">
                                                            <?php
                                                                $arr_month = array("Januari", "Februari", "Maret" ,"April", "Mei", "Juni", "Juli", "Agustus", "Septermber", "Oktober", "November", "Desember");
                                                                foreach ($arr_month as $r_arr_month => $v_arr_month) {
                                                                    echo "<option value=\"".($r_arr_month+1)."\">".$v_arr_month."</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-5">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tahun</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" placeholder="300000" id="th" name="th" value="<?= date("Y");?>" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->

                                            <div class="col-md-2">
                                                <div class="form-group row">
                                                    <div class="col-md-2">
                                                        <button type="submit" class="btn btn-success"  style="vertical-align: bottom;">Filter</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                            </div><br><br>
                <!-- -----------------------------------------------------------------------------------------Data Table-------------------------------------------------------------------------------------- -->
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Data Table</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No. </th>
                                                <th>Tanggal</th>
                                                <th>Sales Net</th>
                                                <th>Struk</th>
                                                <th>Pergantian</th>
                                                <th>Variance</th>
                                                <th>Discount</th>
                                                <th>DSI Hari Ini</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No. </th>
                                                <th>Tanggal</th>
                                                <th>Sales Net</th>
                                                <th>Struk</th>
                                                <th>Pergantian</th>
                                                <th>Variance</th>
                                                <th>Discount</th>
                                                <th>DSI Hari Ini</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php
                                                if(!empty($penjualan)){
                                                    foreach ($penjualan as $r_penjualan => $v_penjualan){
                                                        echo "<tr>
                                                                <td>".($r_penjualan+1)."</td>
                                                                <td>".$v_penjualan->tgl."</td>
                                                                <td align=\"right\">Rp. ".number_format($v_penjualan->sales_net, 2,'.', ',')."</td>
                                                                <td align=\"right\">".number_format($v_penjualan->struk, 0,'.', ',')."</td>
                                                                <td align=\"right\">Rp. ".number_format($v_penjualan->pergantian, 2,'.', ',')."</td>
                                                                <td align=\"right\">Rp. ".number_format($v_penjualan->variance, 2,'.', ',')."</td>
                                                                <td align=\"right\">Rp. ".number_format($v_penjualan->discount, 2,'.', ',')."</td>
                                                                <td align=\"right\">Rp. ".number_format($v_penjualan->dsi_ini, 2,'.', ',')."</td>
                                                                <td>
                                                                    <a href=\"#\" onclick=\"up_penjualan('".$v_penjualan->id_lap."')\"><i class=\"btn btn-info\"><i class=\"fa fa-pencil-square-o\"></i></i></a>
                                                                    <a href=\"#\" onclick=\"del_penjualan('".$v_penjualan->id_lap."')\"><i class=\"btn btn-danger\"><i class=\"fa fa-trash-o\"></i></i></a>
                                                                </td>
                                                            </tr>";
                                                    }
                                                }
                                            ?>

                                        </tbody>
                                    </table><!-- <a href="" ></a> -->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            <!-- sample modal content -->
                                <div class="modal fade bs-example-modal-lg" id="update_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">UPDATE DATA PELAPORAN</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form action="<?= base_url()."user/mainuser/up_penjualan"?>" method="post" class="form-horizontal">
                                            <div class="modal-body">
                                                <div class="form-body">
                                                    <h3 class="box-title">Data 1</h3>
                                                    <hr class="m-t-0 m-b-40">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Tangal Input</label>
                                                                <div class="col-md-9">
                                                                    <input type="date" class="form-control" id="tgl" name="tgl" value="<?php echo date("Y-m-d");?>" required="">
                                                                    <p id="ex_tgl"></p>
                                                                </div>
                                                                

                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Sales Net</label>
                                                                <div class="col-md-9">
                                                                    <input type="number" class="form-control" placeholder="300000" id="up_sales_net" name="sales_net" required="">
                                                                    <p id="ex_up_sales_net"></p>

                                                                </div>
                                                                

                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Struk</label>
                                                                <div class="col-md-9">
                                                                    <input type="number" class="form-control" placeholder="409" id="up_struk" name="struk" required="">
                                                                    <p id="ex_up_struk"></p>
                                                                </div>
                                                                

                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Penggantian</label>
                                                                <div class="col-md-9">
                                                                    <input type="number" class="form-control" placeholder="700000" id="up_penggantian" name="penggantian" required="">
                                                                    <p id="ex_up_penggantian"></p>
                                                                </div>
                                                                

                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Variance</label>
                                                                <div class="col-md-9">
                                                                    <input type="number" class="form-control" placeholder="800000" id="up_variance" name="variance" required="">
                                                                    <p id="ex_up_variance"></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    
                                                    <h3 class="box-title">Data 2</h3>
                                                    <hr class="m-t-0 m-b-40">
                                                    <!--/row-->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Discount</label>
                                                                <div class="col-md-9">
                                                                    <input type="number" class="form-control" placeholder="700000" id="up_disc" name="disc" required="">
                                                                    <p id="ex_up_disc"></p>
                                                                </div>
                                                                

                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">DSI Hari Ini</label>
                                                                <div class="col-md-9">
                                                                    <input type="number" class="form-control" placeholder="800000" id="up_dsi_ini" name="dsi_ini" required="">
                                                                    <p id="ex_up_dsi"></p>
                                                                </div>

                                                                    <!-- $("#up_sales_net").val(); -->
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control" placeholder="800000" id="up_id_lap" name="id_lap" readonly="">
                                                    </div>
                                                </div>    
                                                                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger waves-effect text-left">Sipman</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
            <!--Morris JavaScript -->
            <script src="<?= base_url();?>template_admin/assets/plugins/raphael/raphael-min.js"></script>
            <script src="<?= base_url();?>template_admin/assets/plugins/morrisjs/morris.js"></script>


            <script type="text/javascript">
                var graph_data = [];
                <?php 
                    if (isset($graph_data)){
                        echo "graph_data = JSON.parse('".$graph_data."');";
                    } 
                ?>
                
                $(document).ready(function(){
                    console.log(graph_data);
                    set_chart(graph_data);

                });

                function set_chart(val_data){
                    $(function () {
                    "use strict";
                    // Extra chart
                     Morris.Area({
                            element: 'extra-area-chart',
                            data: val_data,
                                    lineColors: ['#009efb'],
                                    xkey: 'period',
                                    ykeys: ['val_data'],
                                    labels: ['Sales Net'],
                                    pointSize: 0,
                                    lineWidth: 0,
                                    resize:false,
                                    fillOpacity: 0.8,
                                    behaveLikeLine: true,
                                    gridLineColor: '#e0e0e0',
                                    hideHover: 'auto'
                            
                        });
                    }); 
                }
                
                function currency(x){
                    return x.toLocaleString('us-EG');
                }

                function up_penjualan(id_lap){
                    clear_up();
                    var data_main =  new FormData();
                    data_main.append('id_lap', id_lap);    
                    $.ajax({
                        url: "<?php echo base_url()."/user/mainuser/index_up_penjualan/";?>", // point to server-side PHP script 
                        dataType: 'html',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data_main,                         
                        type: 'post',
                        success: function(res){
                            // console.log(res);
                            res_update(res);
                            // $("#out_up_mhs").html(res);
                        }
                    });
                    $("#update_modal").modal('show');
                }

                function res_update(res){
                    var data = JSON.parse(res);
                    console.log(data);

                    if(data.status){
                        var main_data = data.val;
                        $("#tgl").val(main_data.tgl);
                        $("#up_sales_net").val(main_data.sales_net);
                        $("#up_struk").val(main_data.struk);
                        $("#up_penggantian").val(main_data.pergantian);
                        $("#up_variance").val(main_data.variance);
                        $("#up_disc").val(main_data.discount);
                        $("#up_dsi_ini").val(main_data.dsi_ini);
                        $("#up_dsi_ini").val(main_data.dsi_ini);

                        $("#up_id_lap").val(main_data.id_lap);
                        

                        //------------------------------------------------------------------update sales net
                        var masbro = 0;
                        if($("#up_sales_net").val() != null){
                            masbro = "Rp. "+currency(parseInt($("#up_sales_net").val()));    
                            // console.log($("#sales_net";
                        }else{
                            masbro = 0;
                        }
                        $("#ex_up_sales_net").html(masbro);


                        //------------------------------------------------------------------up_penggantian
                        var masbro = 0;
                        if($("#up_penggantian").val() != null){
                            masbro = "Rp. "+currency(parseInt($("#up_penggantian").val()));    
                        }else{
                            masbro = 0;
                        }
                        
                        $("#ex_up_penggantian").html(masbro);


                        //------------------------------------------------------------------up_variance
                        var masbro = 0;
                        if($("#up_variance").val() != null){
                            masbro = "Rp. "+currency(parseInt($("#up_variance").val()));    
                        }else{
                            masbro = 0;
                        }
                        
                        $("#ex_up_variance").html(masbro);


                        //------------------------------------------------------------------up_disc
                        var masbro = 0;
                        if($("#up_disc").val() != null){
                            masbro = "Rp. "+currency(parseInt($("#up_disc").val()));    
                        }else{
                            masbro = 0;
                        }
                        
                        $("#ex_up_disc").html(masbro);


                        //------------------------------------------------------------------up_dsi_ini
                        var masbro = 0;
                        if($("#up_dsi_ini").val() != null){
                            masbro = "Rp. "+currency(parseInt($("#up_dsi_ini").val()));    
                        }else{
                            masbro = 0;
                        }
                        
                        $("#ex_up_dsi").html(masbro);
                        
                    }else{
                        clear_up();
                    }
                }

                function clear_up(){
                        $("#tgl").val("");
                        $("#up_sales_net").val("");
                        $("#up_struk").val("");
                        $("#up_penggantian").val("");
                        $("#up_variance").val("");
                        $("#up_disc").val("");
                        $("#up_dsi_ini").val("");
                        $("#up_dsi_ini").val("");

                        $("#up_id_lap").val("");
                }

                function del_penjualan(id_penjualan){
                    // $("#update_modal").modal('show');
                    var conf = confirm("Apakah anda yakin untuk menghapus "+id_penjualan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_penjualan+" akan terhapus semau.. !!!!!!! ");

                    if(conf){
                        window.location.href = "<?= base_url()."user/mainuser/delete_penjualan/";?>"+id_penjualan;
                    }else{

                    }
                }

                // $("#tgl").keyup(function(){
                //     $("#ex_tgl").html();
                // });

                $("#sales_net").keyup(function(){
                    var masbro = 0;
                    if($("#sales_net").val() != null){
                        masbro = "Rp. "+currency(parseInt($("#sales_net").val()));    
                        // console.log($("#sales_net";
                    }else{
                        masbro = 0;
                    }
                    $("#ex_sales_net").html(masbro);
                });

                // $("#struk").keyup(function(){

                //     $("#ex_struk").html();
                // });

                $("#penggantian").keyup(function(){
                    var masbro = 0;
                    if($("#penggantian").val() != null){
                        masbro = "Rp. "+currency(parseInt($("#penggantian").val()));    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_penggantian").html(masbro);
                });

                $("#variance").keyup(function(){
                    var masbro = 0;
                    if($("#variance").val() != null){
                        masbro = "Rp. "+currency(parseInt($("#variance").val()));    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_variance").html(masbro);
                });

                $("#disc").keyup(function(){
                    var masbro = 0;
                    if($("#disc").val() != null){
                        masbro = "Rp. "+currency(parseInt($("#disc").val()));    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_disc").html(masbro);
                    // $("#ex_disc").html();
                });

                $("#dsi_ini").keyup(function(){
                    var masbro = 0;
                    if($("#dsi_ini").val() != null){
                        masbro = "Rp. "+currency(parseInt($("#dsi_ini").val()));    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_dsi").html(masbro);
                    // $("#ex_dsi").html();
                });


                // --------------------------------------------------------------------update------------------------------------------------------------

                $("#up_sales_net").keyup(function(){
                    var masbro = 0;
                    if($("#up_sales_net").val() != null){
                        masbro = "Rp. "+currency(parseInt($("#up_sales_net").val()));    
                        // console.log($("#sales_net";
                    }else{
                        masbro = 0;
                    }
                    $("#ex_up_sales_net").html(masbro);
                });

                // $("#struk").keyup(function(){

                //     $("#ex_struk").html();
                // });

                $("#up_penggantian").keyup(function(){
                    var masbro = 0;
                    if($("#up_penggantian").val() != null){
                        masbro = "Rp. "+currency(parseInt($("#up_penggantian").val()));    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_up_penggantian").html(masbro);
                });

                $("#up_variance").keyup(function(){
                    var masbro = 0;
                    if($("#up_variance").val() != null){
                        masbro = "Rp. "+currency(parseInt($("#up_variance").val()));    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_up_variance").html(masbro);
                });

                $("#up_disc").keyup(function(){
                    var masbro = 0;
                    if($("#up_disc").val() != null){
                        masbro = "Rp. "+currency(parseInt($("#up_disc").val()));    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_up_disc").html(masbro);
                    // $("#ex_disc").html();
                });

                $("#up_dsi_ini").keyup(function(){
                    var masbro = 0;
                    if($("#up_dsi_ini").val() != null){
                        masbro = "Rp. "+currency(parseInt($("#up_dsi_ini").val()));    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_up_dsi").html(masbro);
                    // $("#ex_dsi").html();
                });

                
            </script>